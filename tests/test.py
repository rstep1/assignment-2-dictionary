import subprocess

BUFFER_OVERFLOW_MESSAGE = "buffer overflow"
KEY_NOT_FOUND_MESSAGE = "key not found"

success_cases = {
    "llpls": "low-lovel programming languages",
    "first_word": "first word explanation",
    "2": "some value 2",
    "lb2": "This is lab number 2",
    "255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-" + \
    "255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-" + \
    "255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-" + \
    "255-symbol-key-255-symbol-key-" : "255 symbol key explanation"
}

error_cases = {
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?" + \
    "bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?bufferoverflow?": BUFFER_OVERFLOW_MESSAGE,
    "key": KEY_NOT_FOUND_MESSAGE,
    "1": KEY_NOT_FOUND_MESSAGE,
    "low-lovel programming languages": KEY_NOT_FOUND_MESSAGE
}

def run_assembly_program(input_data, expected_result):
    try:
        process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout, stderr = process.communicate(input_data)


        if expected_result:
            assert process.returncode == 1, f"The program ended with an error: {stderr}"
            return stdout
        else:
            if stderr == "":
                return  f"The program ended with a message: {stdout}"
            return stderr
        
    except Exception as e:
        return str(e)

def test_program():
    test_number = 1
    success_keys = success_cases.keys()
    for key in success_keys:
        print(f"Test#{test_number}")
        test_success_case(key, test_number)
        test_number += 1

    errror_keys = error_cases.keys()
    for key in errror_keys:
        print(f"Test#{test_number}")
        test_error_case(key, test_number)
        test_number += 1

    print(f"All {test_number} tests completed successfully")

def test_success_case(key, test_number):
    input_data = key
    expected_output = success_cases[key]

    actual_output = run_assembly_program(input_data, True)

    assert actual_output == expected_output, f"Expected '{expected_output}', got '{actual_output}'"
    print("OK")


def test_error_case(key, test_number):
    input_data = key
    expected_error = error_cases[key]

    actual_errror = run_assembly_program(input_data, False)

    assert actual_errror == expected_error, f"Expected '{expected_error}', got '{actual_errror}'"
    print("OK")


if __name__ == "__main__":
    test_program()
