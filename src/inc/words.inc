%include "./src/inc/colon.inc"
section .data

colon "third_word", third_word
db "third word explanation", 0

colon "second_word", second_word
db "second word explanation", 0 

colon "first_word", first_word
db "first word explanation", 0 

colon "1111111111", value_1
db "some value 1", 0 

colon "2", value_2
db "some value 2", 0

colon "123456780", key_title
db "some value 3", 0 

colon "llpls", title1
db "low-lovel programming languages", 0 

colon "lb2", title2
db "This is lab number 2", 0

colon "255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-255-symbol-key-", _255_symbol_title
db "255 symbol key explanation", 0