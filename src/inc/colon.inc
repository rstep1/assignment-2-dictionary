%define ptr 0

%macro colon 2
    %2:
    dq ptr
    db %1, 0
    %define ptr %2
%endmacro