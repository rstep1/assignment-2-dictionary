global exit, string_length, print_string_stdout, print_string_stderr, print_char, \
    print_newline, print_uint, print_int, string_equals, \
    parse_uint, parse_int, read_word, read_char, string_copy

%define EXIT_SYSCALL 60
%define NEWLINE_CHAR `\n`
%define WHITESPACE_CHAR ` `
%define TABULATION_CHAR `\t`
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define WRITE_SYSCALL 1
%define READ_SYSCALL 0
%define STRING_END 0

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rdi], 0
    je   .end
    inc  rdi
    jmp  .counter
  .end:
    sub  rdi, rax
    mov  rax, rdi
    ret

print_string:
    mov  rdx, rax
    mov  rax, WRITE_SYSCALL
    syscall
    ret

print_string_stdout:
    push rdi
    call string_length
    pop rsi
    mov rdi, STDOUT
    jmp print_string

print_string_stderr:
    push rdi
    call string_length
    pop rsi
    mov rdi, STDERR
    jmp print_string

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, NEWLINE_CHAR
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    ;rdi - number
    lea rbx, [rsp] ;save rsp
    mov rcx, 10  ;dividor
    mov rax, rdi
    push 0 ;add terminator
.loop:
    xor rdx, rdx
    div rcx
    add dl, '0' ;to ASCII
    dec rsp
    mov byte [rsp], dl

    test rax, rax
    jne .loop

    lea rdi, [rsp]
    call print_string
    lea rsp, [rbx] ;restore rsp
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    xor rdx, rdx    ;reset flags
    test rax, rax
    jge .not_negative   ;if >= 0
    mov rdi, '-'
    push rax
    call print_char
    pop rax
    imul rax, rax, -1   ;abs
.not_negative:
    mov rdi, rax
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r14
    push r15
    xor rcx, rcx
    xor rax, rax
.loop:
    mov r14b, byte [rsi + rcx]
    mov r15b, byte [rdi + rcx]
    inc rcx
    test r14b, r14b
    je .check_end
    cmp r14b, r15b
    jne .not_equals
    jmp .loop
.check_end:
    test r15b, r15b
    je .equals
    jmp .not_equals
.equals:
    mov rax, 1
    pop r15
    pop r14
    ret
.not_equals:
    xor rax, rax
    pop r15
    pop r14
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rsi
    mov rax, READ_SYSCALL
    mov rdi, STDIN
    lea rsi, [rsp - 1]
    mov rdx, 1
    syscall

    test rax, rax
    jle .end

    mov al, byte [rsp - 1]
    pop rsi
    ret

.end:
    xor rax, rax
    pop rsi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r15
    mov r15, rsi    ;buf len
    mov r12, rdi    ;buf ptr
    xor r13, r13    ;word len
    dec rsi
.loop:
    call read_char

    test al, al
    je .end

    cmp al, WHITESPACE_CHAR
    je .skip_whitespace
    cmp al, TABULATION_CHAR
    je .skip_whitespace
    cmp al, NEWLINE_CHAR
    je .skip_whitespace

    jmp .add_symbol
.skip_whitespace:
    test r13, r13 ;is it begin?
    je .loop
    jmp .end

.add_symbol:
    cmp r13, r15 ;buf overflow
    je .fault

    mov [r12 + r13], al ;add symbol
    inc r13

    jmp .loop
.fault:
    xor rax, rax
    pop r15
    pop r12
    pop r13
    ret
.end:
    mov byte [r12 + r13], 0 ;terminator
    mov rax, r12
    mov rdx, r13
    pop r15
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ;rdi - str ptr
    xor rax, rax
    xor rdx, rdx
.check_number:
    xor rcx, rcx
    mov cl, byte [rdi + rdx]
    test cl, cl
    je .end
    cmp cl, '0' ;if < 0
    jb .end
    cmp cl, '9' ;if > 9
    ja .end
    inc rdx
    sub cl, '0'
    imul rax, rax, 10
    add rax, rcx
    jmp .check_number
.end:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte [rdi], 0
    je .end
    cmp byte [rdi], '-'
    je .negative
    call parse_uint
    ret
.negative:
    inc rdi
    call parse_uint
    inc rdx
    imul rax, rax, -1
.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ;rdi - string ptr
    ;rsi - buf ptr
    ;rdx - buf len
    push r13
    push r14
    push r15
    mov r13, rsi
    mov r14, rdi
    mov r15, rdx

    call string_length
    cmp rax, r15
    jae .fault
    xor rcx, rcx
.loop:
    cmp rcx, rax
    ja .done
    push rax
    mov al, [r14 + rcx]
    mov [r13 + rcx], al
    pop rax
    inc rcx
    jmp .loop
.done:
    pop r15
    pop r14
    pop r13
    ret
.fault:
    xor rax, rax
    pop r15
    pop r14
    pop r13
    ret


