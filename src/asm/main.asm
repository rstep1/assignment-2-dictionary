%include "./src/inc/words.inc"
%include "./src/inc/dict.inc"
%include "./src/inc/lib.inc"

%define BUFFER_LEN 256
%define DICT_PTR_LENGTH 8

section .data
buffer: times 256 db '0xB'

.rodata:
buffer_overflow: db "buffer overflow", 0
word_not_found: db "key not found", 0

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, BUFFER_LEN
    call read_word
    test rax, rax   ;rax - buffer ptr
    jz .buffer_overflow
    mov rdi, ptr    ; ptr on dict
    mov rsi, rax    ; ptr on str
    call find_word
    test rdi, rdi   ;0 - not found
    jz .word_not_found 
    add rdi, DICT_PTR_LENGTH ;skip ptr length
    push rdi
    call string_length
    pop rdi
    add rdi, rax        ;skip key
    inc rdi             ;skip 0-terminator
    call print_string_stdout ;output value
    jmp .end
    .buffer_overflow:
        mov rdi, buffer_overflow
        call print_string_stderr
        jmp .end
    .word_not_found:
        mov rdi, word_not_found
        call print_string_stderr
    .end:
        call exit
