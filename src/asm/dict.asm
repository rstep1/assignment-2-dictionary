%include "./src/inc/lib.inc"

global find_word

find_word:
    ;rdx - str ptr
    ;rdi - dict ptr
    mov r8, rdi ;dict ptr
    .loop:
        lea rdi, [r8 + 8]  ; get key
        call string_equals
        test rax, rax
        jnz .success
        mov r8, [r8] ; udpate dict ptr
        test r8, r8
        jz .fail
        jmp .loop
    .fail:
        xor r8, r8 ; return 0
    .success:
        mov rdi, r8 ; return addr in rdi
        ret

        