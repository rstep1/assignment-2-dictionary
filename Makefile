SRC_DIR = ./src/asm
BIN_DIR = ./bin

NASM = nasm -felf64

SRCS = main.asm lib.asm dict.asm
OBJS = $(BIN_DIR)/main.o $(BIN_DIR)/lib.o $(BIN_DIR)/dict.o

program: $(OBJS)
	ld -o program $(OBJS)

$(BIN_DIR)/%.o: $(SRC_DIR)/%.asm $(BIN_DIR)/lib.o
	@mkdir -p $(BIN_DIR)
	$(NASM) -o $@ $<

$(BIN_DIR)/lib.o: $(SRC_DIR)/lib.asm
	@mkdir -p $(BIN_DIR)
	$(NASM) -o $@ $<

.PHONY: clean
clean:
	rm $(BIN_DIR)/*.o program

.PHONY: test
test:
	python3 ./tests/test.py
